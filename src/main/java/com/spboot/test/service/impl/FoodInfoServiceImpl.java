package com.spboot.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spboot.test.entity.FoodInfo;
import com.spboot.test.repository.FoodInfoRepository;
import com.spboot.test.service.FoodInfoService;

@Service
public class FoodInfoServiceImpl implements FoodInfoService {
	
	@Autowired
	private FoodInfoRepository foodInfoRepo;
	

	@Override
	public List<FoodInfo> getFoodInfoList() {
		// TODO Auto-generated method stub
		return foodInfoRepo.findAll();
	}

}
