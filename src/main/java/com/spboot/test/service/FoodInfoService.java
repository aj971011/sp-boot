package com.spboot.test.service;

import java.util.List;

import com.spboot.test.entity.FoodInfo;

public interface FoodInfoService {
	
	public List<FoodInfo> getFoodInfoList();

}
