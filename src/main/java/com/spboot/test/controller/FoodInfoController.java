package com.spboot.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spboot.test.entity.FoodInfo;
import com.spboot.test.service.FoodInfoService;

public class FoodInfoController {
	
	@Autowired
	private FoodInfoService foodInfoService;
	
	@GetMapping("/food-infos")
	public @ResponseBody List<FoodInfo> getFoodInfos(){
		return foodInfoService.getFoodInfoList();
	}

}
