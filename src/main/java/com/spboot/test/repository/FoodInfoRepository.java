package com.spboot.test.repository;

import java.util.List;

import com.spboot.test.entity.FoodInfo;

public interface FoodInfoRepository {
	
	public List<FoodInfo> findAll();

}
